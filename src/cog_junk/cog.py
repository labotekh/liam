import logging
import random
from urllib.parse import quote

from asgiref.sync import sync_to_async

from bs4 import BeautifulSoup

from discord import app_commands, errors, Interaction, Message, MessageType
from discord.ext import commands

from django.utils.translation import gettext as _

import requests

from cog_junk import URL_CAT, URL_SLOGAN, NO_REF
from cog_junk.models import Reply

logger = logging.getLogger('default')


def get_slogan(word: str) -> str:
    """Get a random slogan"""
    response = requests.get(URL_SLOGAN.format(quote(word)), timeout=5)
    soup = BeautifulSoup(response.text, 'html.parser')
    slogan = [mantra.text for mantra in soup.find_all("span", "business-name")][0]
    return slogan


def randchar(char: str) -> str:
    """Replace randomly a char to cancerize a string"""
    # nosemgrep
    if random.random() > 0.5: # nosec
        char = char.upper()
        # nosemgrep
        if char == 'E' and random.random() < 0.25: # nosec
            return '3'
        # nosemgrep
        if char == 'O' and random.random() < 0.4: # nosec
            return '0'
        # nosemgrep
        if char == 'S' and random.random() < 0.2: # nosec
            return '$'
        return char
    return char.lower()


class JunkCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    GREETINGS = ['bonjour', 'bjr', 'slt', 'yo', 'cc', 'coucou', 'hello']
    QUOI_FEUR = ["quoi", "coi", "koa", "koi"]
    YOLO = ["yolo"]

    @app_commands.command()
    async def fight(self, interaction: Interaction) -> None:
        """ Return "(ง'̀-'́)ง". """
        logger.info(_('JUNK_LOG_FIGHT'), {'user': interaction.user})
        await interaction.response.send_message("(ง'̀-'́)ง")

    @app_commands.command()
    async def caner(self, interaction: Interaction) -> None:
        """
        Mes aievx, qvelle fort belle jovrnee povr ressentir
        en mien coevr vne si belle envie de caner.
        """
        logger.info(_('JUNK_LOG_CANER'))
        await interaction.response.send_message(_('JUNK_CANER'))

    @app_commands.command()
    async def cat(self, interaction: Interaction) -> None:
        """ Return random cat photo from api.thecatapi.com. """
        url = requests.get(URL_CAT, timeout=5).json()[0]['url']
        await interaction.response.send_message(url)
        logger.info(_('JUNK_LOG_CAT'), {'user': interaction.user, 'url': url})

    @app_commands.command()
    async def slogan(self, interaction: Interaction, expression: str) -> None:
        """ Return a random slogan with the provided expression. """
        logger.info(_('JUNK_LOG_SLOGAN'), {'user': interaction.user, 'expression': expression})
        await interaction.response.send_message(get_slogan(expression))

    @app_commands.command()
    async def cancer(self, interaction: Interaction, expression: str) -> None:
        """ Return a cancerized string. """
        logger.info(_('JUNK_LOG_CANCER'), {'user': interaction.user, 'expression': expression})
        await interaction.response.send_message(''.join(map(randchar, expression)))

    @app_commands.command()
    async def no_ref(self, interaction: Interaction) -> None:
        """ Return a string to show your cruel lack of references. """
        logger.info(_('JUNK_LOG_NO_REF'), {'user': interaction.user})
        await interaction.response.send_message(NO_REF % {
            'joke': _('JUNK_NO_REF_JOKE'),
            'me': _('JUNK_NO_REF_ME')
        })

    @commands.Cog.listener()
    async def on_message(self, message: Message) -> None:
        if message.type != MessageType.chat_input_command:
            try:
                message = await message.fetch()
            except errors.NotFound:
                return

            content = message.content.lower()
            # sanitize the message by removing punctuation
            for punctuation_char in '.:;,?!"\'()[]':
                content = content.replace(punctuation_char, '')
            word_array = content.split(' ')
            
            if not message.author.bot:
                reply = await sync_to_async(Reply.objects.get_random)()
                if reply and any(self.bot.user.id == user.id for user in message.mentions):
                    await message.channel.send(reply.message)

                if "liam" in word_array:
                    await message.channel.send(get_slogan("Liam"))
                if "rip" in word_array:
                    await message.add_reaction('🪦')

            if any(txt in word_array for txt in self.GREETINGS):
                await message.add_reaction('👋')
                logger.info(_('JUNK_LOG_GREETINGS'), {'user': message.author})
            if any(txt in word_array for txt in self.YOLO):
                await message.add_reaction('🖕')
                await message.add_reaction('💩')
                logger.info(_('JUNK_LOG_YOLO'), {'user': message.author})

            got_suffix = any(word_array[-1].endswith(suffix) for suffix in self.QUOI_FEUR)
            if got_suffix and random.random() < 0.33:
                await message.add_reaction('🇫')
                await message.add_reaction('🇪')
                await message.add_reaction('🇺')
                await message.add_reaction('🇷')
                logger.info(_('JUNK_LOG_QUOI_FEUR'), {'user': message.author})
