"""
Administration panel configuration for cog_junk
"""

from django.contrib import admin

from cog_junk.models import Reply


@admin.register(Reply)
class AdminReply(admin.ModelAdmin):
    """Settings to display `Reply` in administration panel. """

    list_display = ('message',)
    list_filter = ('message',)
    ordering = ('message',)
    search_fields = ('message',)
    fields = ('message',)
