"""Configuration for cog_junk app. """

from django.apps import AppConfig


class CogJunkConfig(AppConfig):
    """Configuration object for cog_junk app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_junk'
