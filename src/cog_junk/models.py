from django.db import models


class ReplyManager(models.Manager): # pylint: disable=too-few-public-methods
    """A manager for the Reply model. """

    def get_random(self):
        """Get a random Reply. """
        return self.get_queryset().order_by('?').first()


class Reply(models.Model):
    """A reply sent by the Discord bot when mentionned. """
    message = models.CharField(max_length=100, unique=True)
    objects = ReplyManager()

    def __str__(self) -> str:
        return str(self.message)
