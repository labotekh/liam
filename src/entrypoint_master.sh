#!/bin/sh

printf "%s" "waiting for database ..."
while ! nc -z db 5432
do
	sleep 1
    printf "%c" "."
done
printf "\n%s\n"  "Database is online"

mkdir -p /code/files/logs
python3 manage.py makemigrations --no-input
python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input

python3 manage.py default_admin

chown -R 8135:8135 /code
# run CMD
exec runuser -u runner -g runner -- $@
