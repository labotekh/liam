"""Configuration for cog_poll app. """

from django.apps import AppConfig


class CogPollConfig(AppConfig):
    """Configuration object for cog_poll app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_poll'
