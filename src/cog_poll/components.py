from typing import List

from discord import Interaction, TextStyle, SelectOption
from discord.ui import TextInput, Modal, Select

from django.utils.translation import gettext as _


class NewChoiceModal(Modal):

    title = _('POLL_NEW_CHOICE_MODAL_TITLE')
    choice = TextInput(
        label=_('POLL_NEW_CHOICE_LABEL'),
        style=TextStyle.short, min_length=1, max_length=100
    )

    def __init__(self, view):
        super().__init__()
        self.view = view

    async def on_submit(self, inter: Interaction) -> None: # pylint: disable=arguments-differ
        error = self.view.add_choice(self.choice.value)
        await self.view.update(inter, error)


class EditNameDescriptionModal(Modal):

    title = _('POLL_NAME_MODAL_TITLE')
    name = TextInput(
        label=_('POLL_NAME_MODAL_LABEL'),
        style=TextStyle.short, max_length=100
    )
    description = TextInput(
        label=_('POLL_DESCRIPTION_MODAL_LABEL'),
        style=TextStyle.paragraph, max_length=500
    )

    def __init__(self, view):
        super().__init__()
        self.view = view
        self.name.default = self.view.data.name
        self.description.default = self.view.data.description

    async def on_submit(self, inter: Interaction) -> None: # pylint: disable=arguments-differ
        self.view.data.name = self.name.value
        self.view.data.description = self.description.value
        await self.view.update(inter)


class EditChoicesModal(Modal):

    title = _('POLL_EDIT_CHOICES_MODAL_TITLE')

    def __init__(self, view):
        super().__init__()
        self.view = view
        page = self.view.data.get_page()
        for choice in self.view.data.choices[page['start']-1:page['end']]:
            text = TextInput(label=choice, default=choice, style=TextStyle.short, max_length=100)
            self.add_item(text)

    async def on_submit(self, inter: Interaction) -> None: # pylint: disable=arguments-differ
        values = [child.value for child in self.children]
        error = self.view.data.set_choices(values)
        await self.view.update(inter, error)


class EditSelect(Select):

    def __init__(self, row: int = 0):
        super().__init__(placeholder=_('POLL_DELETE_CHOICE_PLACEHOLDER'), row=row)

    def update(self) -> None:
        if self.view:
            self.options = [SelectOption(label=choice) for choice in self.view.data.choices]
            self.max_values = len(self.options)

    async def callback(self, interaction: Interaction) -> None:
        for choice in self.values:
            self.view.remove_choice(choice)
        if self.view:
            await self.view.update(interaction)


class VoteSelect(Select):

    def __init__(self, values: List[str], multi: bool = False, row: int = 0):
        options = [SelectOption(label=value) for value in values]
        if multi:
            max_values = len(options)
        else:
            max_values = 1
        super().__init__(
            placeholder=_('POLL_VOTE_CHOICE_PLACEHOLDER'),
            options=options, max_values=max_values, row=0
        )

    async def callback(self, interaction: Interaction) -> None:
        await self.view.update_votes(interaction, self.values)
