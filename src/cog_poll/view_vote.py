import logging
from typing import List

from discord import Interaction, Embed
from discord.ui import View

from django.utils.translation import gettext as _

from liam.utils.discord_components import ActionButton

from cog_poll.utils import PollData
from cog_poll.components import VoteSelect

logger = logging.getLogger('default')


class VoteView(View):

    data    : PollData
    votes   : dict
    voters  : list
    closed  : bool

    s_votes : VoteSelect
    b_close : ActionButton

    def __init__(self, data):
        super().__init__(timeout=None)
        self.data = data
        self.votes = {}
        self.voters = []
        self.closed = False

        self.s_votes = VoteSelect(self.data.choices, multi=self.data.multi)
        self.b_close = ActionButton('✅', self.close, _('POLL_CLOSE_LABEL'))
        self.add_item(self.s_votes)
        self.add_item(self.b_close)

    # ======================================================================================= #
    #                                      DISPLAY METHODS                                    #
    # ======================================================================================= #

    def show_options(self, embed: Embed) -> None:
        str_public = _('POLL_PUBLIC_FOOTER') % {'show': '✅' if self.data.public      else '❌'}
        str_temp   = _('POLL_TEMP_FOOTER')   % {'show': '✅' if self.data.temp        else '❌'}
        str_multi  = _('POLL_MULTI_FOOTER')  % {'show': '✅' if self.data.multi       else '❌'}
        str_author = _('POLL_AUTHOR_FOOTER') % {'show': '✅' if self.data.show_author else '❌'}

        str_voters = _('POLL_VOTERS') % {'voters': len(self.voters)}

        str_footer = f"{str_voters}\n{str_public}\n{str_temp}\n{str_multi}\n{str_author}"
        if self.data.show_author:
            embed.set_footer(text=str_footer, icon_url=self.data.author.display_avatar)
        else:
            embed.set_footer(text=str_footer)

    def show_votes(self, embed: Embed) -> None:
        for vote, values in self.votes.items():
            str_vote = f'**{len(values)}**'
            if self.data.public:
                str_vote += ' : ' + ''.join([user.mention for user in values])
            embed.add_field(name=vote, value=str_vote, inline=False)

    @property
    def embed(self) -> Embed:
        embed = Embed(colour=0x4ad006, title=self.data.name, description=self.data.description)
        self.show_options(embed)
        if self.data.temp or self.closed:
            self.show_votes(embed)
        return embed

    # ======================================================================================= #
    #                                     WRAPPING METHODS                                    #
    # ======================================================================================= #

    async def call_action(self, inter: Interaction, action: callable) -> None:
        if inter.user == self.data.author:
            await action(inter)
        else:
            await inter.response.send_message(content=_('POLL_CLOSE_ERROR'), ephemeral=True)

    # ======================================================================================= #
    #                                      ACTION METHODS                                     #
    # ======================================================================================= #

    async def close(self, inter: Interaction) -> None:
        self.closed = True
        self.remove_item(self.s_votes)
        self.remove_item(self.b_close)
        self.timeout = 0
        await inter.response.edit_message(embed=self.embed, view=None)
        logger.info(_('POLL_LOG_CLOSE_VOTE'), {
            'name': self.data.name,
            'author': self.data.author
        })

    async def update_votes(self, inter: Interaction, votes: List[str]) -> None:
        if not self.data.multi and len(votes) != 1:
            await inter.response.send_message(content=_('POLL_MULTI_VOTE_ERROR'), ephemeral=True)
        elif not self.closed:
            if inter.user not in self.voters:
                self.voters.append(inter.user)
            if self.data.multi:
                for vote in votes:
                    if vote not in self.votes:
                        self.votes[vote] = []
                    if inter.user not in self.votes[vote]:
                        self.votes[vote].append(inter.user)
            else:
                for vote, values in self.votes.items():
                    if inter.user in values:
                        values.remove(inter.user)
                if votes[0] not in self.votes:
                    self.votes[votes[0]] = []
                self.votes[votes[0]].append(inter.user)
            await inter.response.edit_message(embed=self.embed, view=self)
        else:
            await inter.response.edit_message(embed=self.embed)
