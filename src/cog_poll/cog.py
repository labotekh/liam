import logging
from typing import Optional

from discord import app_commands, Interaction
from discord.ext import commands

from django.utils.translation import gettext as _

from cog_poll.view_create import CreateView

logger = logging.getLogger('default')


class PollCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @app_commands.command()
    @app_commands.guild_only()
    async def poll(self, inter: Interaction, name: str, description: Optional[str], # pylint: disable=too-many-arguments,unused-argument
        public: Optional[bool] = False, temp: Optional[bool] = False, # pylint: disable=unused-argument
        multi: Optional[bool] = False, show_author: Optional[bool] = False) -> None: # pylint: disable=unused-argument
        """Show a form to create a poll.

        Parameters
        -----------
        name: str
            The name of the poll
        description: str, optional
            A description of the poll
        public: bool, optional
            Wheter the vote of each member is public or not
        temp: bool, optional
            Wheter the poll will display intermediate results or not
        multi: bool, optional
            Wheter a member can choose multiple options or not
        show_author: bool, optional
            Wheter the poll display the author or not
        """
        logger.info(_('POLL_LOG_START_CREATE'), {'user': inter.user})
        view = CreateView(data=locals())
        await inter.response.send_message(embed=view.embed, view=view, ephemeral=True)
