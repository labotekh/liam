from typing import List

from discord import Member

from django.utils.translation import gettext as _


class PollData: # pylint: disable=too-many-instance-attributes

    author: Member
    name: str
    description: str
    choices: List[str]
    page: int
    public: bool
    temp: bool
    multi: bool
    show_author: bool

    def __init__(self, data):

        self.author = data['inter'].user
        self.name = data['name']
        self.description = data['description']
        self.public = data['public']
        self.temp = data['temp']
        self.multi = data['multi']
        self.show_author = data['show_author']
        self.choices = []
        self.page = 0

    @property
    def is_empty(self) -> bool:
        return len(self.choices) == 0

    def paginate(self) -> None:
        if len(self.choices) > (self.page + 1) * 5:
            self.page += 1
        else:
            self.page = 0

    def add_choice(self, value: str) -> str:
        error = ''
        if len(self.choices) >= 25:
            error = _('POLL_ADD_CHOICE_ERROR_TOO_MANY')
        elif len(value) > 100:
            error = _('POLL_ADD_CHOICE_ERROR_TOO_LONG') % {'length': len(value)}
        elif value in self.choices:
            error = _('POLL_ADD_CHOICE_ERROR_DUPPLICATE') % {'value': value}
        else:
            self.choices.append(value)
        return error

    def remove_choice(self, value: str) -> None:
        self.choices.remove(value)
        if len(self.choices) < (self.page + 1) * 5 and self.page != 0:
            self.page -= 1

    def set_choices(self, values: List[str]) -> str:
        page = self.get_page()
        error = None
        for index, value in enumerate(values):
            if value not in self.choices:
                self.choices[page['start'] - 1 + index] = value
            else:
                error = _('POLL_ADD_CHOICE_ERROR_DUPPLICATE') % {'value': value}
                break
        return error

    def get_page(self) -> dict:
        start = 1 + self.page * 5
        end = (self.page + 1) * 5 if len(self.choices) > (self.page + 1) * 5 else len(self.choices)
        return {'start': start, 'end': end}
