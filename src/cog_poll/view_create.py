import logging
from typing import Optional

from discord import Interaction, Embed
from discord.ui import View

from django.utils.translation import gettext as _

from liam.utils.discord_components import ActionButton, SwitchButton

from cog_poll import components
from cog_poll.utils import PollData
from cog_poll.view_vote import VoteView

logger = logging.getLogger('default')


class CreateView(View): # pylint: disable=too-many-instance-attributes

    data          : PollData
    validated     : bool

    s_choices     : components.EditSelect

    b_name        : ActionButton
    b_add         : ActionButton
    b_choices     : ActionButton
    b_next        : ActionButton

    b_cancel      : ActionButton
    b_validate    : ActionButton

    b_public      : SwitchButton
    b_temp        : SwitchButton
    b_multi       : SwitchButton
    b_show_author : SwitchButton

    def __init__(self, data):
        super().__init__(timeout=None)

        self.data = PollData(data=data)
        self.validated = False

        self.init_components()
        self.add_components()

    # ======================================================================================= #
    #                                     COMPONENT METHODS                                   #
    # ======================================================================================= #

    def init_components(self):
        self.s_choices     = components.EditSelect()

        self.b_name        = ActionButton('✏️', self.name,     _('POLL_EDIT_NAME_LABEL'),    row=1)
        self.b_add         = ActionButton('➕', self.add,      _('POLL_ADD_LABEL'),          row=1)
        self.b_choices     = ActionButton('📝', self.choices,  _('POLL_EDIT_CHOICES_LABEL'), row=1)
        self.b_next        = ActionButton('⏭️', self.next,                                   row=1)

        self.b_cancel      = ActionButton('❌', self.cancel,   _('POLL_CANCEL_LABEL'),       row=3)
        self.b_validate    = ActionButton('✅', self.validate, _('POLL_VALIDATE_LABEL'),     row=3)

        self.b_public      = SwitchButton('👁️', 'public',      _('POLL_PUBLIC_LABEL'), row=2)
        self.b_temp        = SwitchButton('ℹ️', 'temp',        _('POLL_TEMP_LABEL'),   row=2)
        self.b_multi       = SwitchButton('📂', 'multi',       _('POLL_MULTI_LABEL'),  row=2)
        self.b_show_author = SwitchButton('🕵️', 'show_author', _('POLL_AUTHOR_LABEL'), row=2)

    def add_components(self):
        switchs = [self.b_public, self.b_temp, self.b_multi, self.b_show_author]
        buttons = [
            self.b_name, self.b_add, self.b_choices,
            self.b_next, self.b_cancel, self.b_validate
        ]

        for component in buttons + switchs:
            self.add_item(component)

        for switch in switchs:
            switch.update()
            switch.update()
        self.b_validate.disabled = True

    # ======================================================================================= #
    #                                       DATA METHODS                                      #
    # ======================================================================================= #

    @property
    def embed(self) -> Embed:
        return Embed(colour=0x4ad006, title=self.data.name, description=self.data.description)

    def add_choice(self, value: str) -> str:
        error = self.data.add_choice(value)
        if not self.data.is_empty and self.s_choices not in self.children:
            self.add_item(self.s_choices)
        return error

    def remove_choice(self, value: str) -> None:
        self.data.remove_choice(value)
        if self.data.is_empty:
            self.remove_item(self.s_choices)

    def paginate_button(self) -> None:
        if len(self.data.choices) > 5:
            self.b_choices.label = _('POLL_EDIT_CHOICES_LABEL_AWARE') % self.data.get_page()
        else:
            self.b_choices.label = _('POLL_EDIT_CHOICES_LABEL')

    # ======================================================================================= #
    #                                     WRAPPING METHODS                                    #
    # ======================================================================================= #

    async def call_action(self, inter: Interaction, action: str) -> None:
        if inter.user == self.data.author:
            await action(inter)
        else:
            await inter.response.send_message(content=_('POLL_EDIT_ERROR'), ephemeral=True)

    async def update(self, inter: Interaction, error: Optional[str] = None) -> None:
        embed = self.embed
        if error and embed:
            embed.set_footer(text='⚠️ ' + error)
        if len(self.data.choices) < 2 or self.validated:
            self.b_validate.disabled = True
        else:
            self.b_validate.disabled = False
        self.s_choices.update()
        self.paginate_button()
        await inter.response.edit_message(embed=embed, view=self)

    # ======================================================================================= #
    #                                     BUTTON ACTIONS                                      #
    # ======================================================================================= #

    async def cancel(self, inter: Interaction) -> None:
        self.timeout = 0
        for component in self.children:
            component.disabled = True
        if self.validated:
            await self.update(inter, _('POLL_VALIDATED'))
        else:
            await self.update(inter, _('POLL_CANCEL'))

    async def add(self, inter: Interaction) -> None:
        await inter.response.send_modal(components.NewChoiceModal(view=self))

    async def validate(self, inter: Interaction) -> None:
        if self.validated:
            await self.cancel(inter)
        else:
            self.timeout = 0
            self.validated = True
            view = VoteView(self.data)
            await inter.response.send_message(embed=view.embed, view=view)
            logger.info(_('POLL_LOG_START_VOTE'), {
                'name': self.data.name,
                'author': self.data.author
            })

    async def name(self, inter: Interaction) -> None:
        await inter.response.send_modal(components.EditNameDescriptionModal(view=self))

    async def choices(self, inter: Interaction) -> None:
        if not self.data.is_empty:
            await inter.response.send_modal(components.EditChoicesModal(view=self))
        else:
            await self.update(inter, _('POLL_EDIT_CHOICES_ERROR'))

    async def next(self, inter: Interaction) -> None:
        self.data.paginate()
        await self.update(inter)
