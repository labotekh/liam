import logging
import os
import tempfile
from typing import List

from django.conf import settings
from django.core.files import File
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext as _

import requests

logger = logging.getLogger('default')


class Image(models.Model):
    """An image to be associated to one or multiple pools."""

    image = models.ImageField(upload_to='images')

    def __str__(self) -> str:
        return str(self.image.name)


class ImagePoolManager(models.Manager):
    """A manager for the ImagePool model. """

    def get_image(self, pool_name: str) -> str:
        """Retrieve an image from the specified pool. """
        url = None
        pool = self.get_queryset().filter(name=pool_name).first()
        if pool:
            image = pool.pool.order_by('?').first()
            if image:
                url = f'https://{settings.ALLOWED_HOSTS[0]}{image.image.url}'
        return url

    def autocomplete_pool_name(self, current: str) -> List[str]:
        """Provide a selection of possible pools remaining with the current entry. """
        return list(self.get_queryset().filter(name__startswith=current))

    def create_image(self, pool_name: str, url: str) -> str:
        """Create an image from url in the specified pool. """
        pool = self.get_queryset().filter(name=pool_name).first()
        response = _('IMAGE_UNKNOWN_POOL') % {'pool': pool_name}
        if pool:
            response = requests.get(url, stream=True, timeout=5)
            response = _('IMAGE_CREATE_FAIL_TO_DOWNLOAD')
            if response.status_code == requests.codes.ok: # pylint: disable=no-member
                with tempfile.NamedTemporaryFile() as destination:
                    for block in response.iter_content(1024 * 8):
                        if not block:
                            break
                        destination.write(block)
                    timestamp = timezone.now().strftime('%Y-%m-%d_%H-%M-%S')
                    ext = url.split('.')[-1].split('/')[0]
                    file_name = f"{timestamp}.{ext}"
                    image = Image(image=File(destination, name=file_name))
                    image.save()
                    pool.pool.add(image)
                    response = _('IMAGE_CREATE_SUCCESS')
        return response

    def create_pool(self, pool_name: str) -> str:
        """Create a new pool with the specified name. """
        response = _('IMAGE_POOL_ALREADY_EXISTS')
        pool = self.get_queryset().filter(name=pool_name).first()
        if not pool:
            self.model(name=pool_name).save()
            response = _('IMAGE_POOL_CREATED')
        return response

    def delete_pool(self, pool_name: str) -> str:
        """Delete the specified pool """
        response = _('IMAGE_UNKNOWN_POOL') % {'pool': pool_name}
        pool = self.get_queryset().filter(name=pool_name).first()
        if pool:
            pool.delete()
            response = _('IMAGE_POOL_DELETED')
        return response


class ImagePool(models.Model):
    """A pool of images with a name."""
    name = models.CharField(max_length=100, unique=True)
    pool = models.ManyToManyField(Image)
    objects = ImagePoolManager()

    def __str__(self) -> str:
        return str(self.name)


@receiver(post_delete, sender=Image)
def delete_image_file(sender, instance: Image, **kwargs) -> None: # pylint: disable=unused-argument
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)
