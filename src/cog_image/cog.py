import logging
from typing import Optional, List
import unidecode

from asgiref.sync import sync_to_async

from discord import app_commands, Interaction, Embed
from discord.ext import commands

from django.utils.translation import gettext as _

from cog_image.models import ImagePool

logger = logging.getLogger('default')


class ImageCog(commands.Cog):
    """ A Cog to manage Image related commands. """

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    # pylint: disable=unused-argument
    async def pool_autocomplete(self, interaction: Interaction,
        current: str) -> List[app_commands.Choice[str]]:
        """ Autocomplete pool name. """
        result = await sync_to_async(ImagePool.objects.autocomplete_pool_name)(current)
        return [app_commands.Choice(name=pool.name, value=pool.name) for pool in result]

    @app_commands.command()
    @app_commands.autocomplete(pool=pool_autocomplete)
    async def image(self, interaction: Interaction, pool: str) -> None:
        """ Return a random image from the selected pool. """
        pool = pool.lower()
        url = await sync_to_async(ImagePool.objects.get_image)(pool)
        user = interaction.user # pylint: disable=possibly-unused-variable
        if url:
            logger.info(_('IMAGE_LOG_SEND_IMAGE_FROM_POOL'), locals())
            await interaction.response.send_message(url)
        else:
            logger.info(_('IMAGE_LOG_UNKNOWN_OR_EMPY_POOL'), locals())
            embed = Embed(title=_('IMAGE_UNKNOWN_POOL') % locals())
            await interaction.response.send_message(embed=embed, ephemeral=True)

    @app_commands.command()
    @app_commands.autocomplete(pool=pool_autocomplete)
    async def image_add(self, interaction: Interaction, pool: str, url: str) -> None:
        """ Add an image to the selected pool (Please don't use as attack vector). """
        pool = pool.lower()
        response = await sync_to_async(ImagePool.objects.create_image)(pool, url)
        user = interaction.user # pylint: disable=possibly-unused-variable
        logger.info(_('IMAGE_LOG_ADD_IMAGE_TO_POOL'), locals())
        await interaction.response.send_message(embed=Embed(title=response), ephemeral=True)

    @app_commands.command()
    async def image_pool(self, interaction: Interaction, pool: str,
        delete: Optional[bool] = False) -> None:
        """ Create or delete a pool. """
        pool = unidecode.unidecode(pool.lower())
        response = None
        user = interaction.user # pylint: disable=possibly-unused-variable
        if delete:
            response = await sync_to_async(ImagePool.objects.delete_pool)(pool)
            logger.info(_('IMAGE_LOG_DELETE'), locals())
        else:
            response = await sync_to_async(ImagePool.objects.create_pool)(pool)
            logger.info(_('IMAGE_LOG_CREATE'), locals())
        await interaction.response.send_message(embed=Embed(title=response), ephemeral=True)
