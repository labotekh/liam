"""
Administration panel configuration for cog_image
"""

from django.contrib import admin
from django.utils.html import format_html

from cog_image.models import Image, ImagePool


@admin.action(description="Delete unlinked images")
def delete_unlinked_images(modeladmin, request, queryset): # pylint: disable=unused-argument
    """Delete all images not used in a pool. """
    queryset.filter(imagepool__isnull=True).delete()


@admin.register(Image)
class AdminImage(admin.ModelAdmin):
    """Settings to display `Image` in administration panel. """

    list_display = ('name', 'preview')
    list_filter = ('image',)
    ordering = ('image',)
    search_fields = ('image',)
    fields = ('image', 'preview')
    readonly_fields = ('preview',)
    actions = [delete_unlinked_images]

    def name(self, obj):
        """ Show image name. """
        return obj.image.name

    def preview(self, obj):
        """ Show image preview. """
        preview_str = '-'
        if obj.pk:
            preview_str = format_html(f'<img src="/media/{obj.image}" width="70" loading="lazy"/>')
        return preview_str


@admin.register(ImagePool)
class AdminImagePool(admin.ModelAdmin):
    """Settings to display `ImagePool` in administration panel. """

    list_display = ('name', 'preview')
    list_filter = ('name',)
    ordering = ('name',)
    search_fields = ('name',)
    fields = ('name', 'pool')

    def preview(self, obj):
        """ Show image preview. """
        preview_str = ''
        style = 'padding-right:5px'
        for image in obj.pool.all():
            preview_str += f'<img src="/media/{image}" style="{style}" width="70" loading="lazy"/>'
        return format_html(preview_str)
