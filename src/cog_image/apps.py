"""Configuration for cog_image app. """

from django.apps import AppConfig


class CogImageConfig(AppConfig):
    """Configuration object for cog_image app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_image'
