"""Configuration for cog_tictactoe app. """

from django.apps import AppConfig


class CogTicTacToeConfig(AppConfig):
    """Configuration object for cog_tictactoe app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_tictactoe'
