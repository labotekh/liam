import logging
from typing import List

from discord import app_commands, Member, Interaction, ButtonStyle
from discord.ext import commands
from discord.ui import View, Button

from django.utils.translation import gettext as _

logger = logging.getLogger('default')


class TicTacToeButton(Button['TicTacToe']):

    x_coord: int
    y_coord: int

    def __init__(self, x_coord: int, y_coord: int):
        super().__init__(style=ButtonStyle.secondary, label='\u200b', row=y_coord)
        self.x_coord = x_coord
        self.y_coord = y_coord

    @property
    def state(self) -> int:
        return self.view.board[self.y_coord][self.x_coord] if self.view else None

    async def callback(self, interaction: Interaction)  -> None:
        if self.view and self.state not in self.view.players:
            await self.view.update(interaction, self)


class TicTacToe(View):

    children: List[TicTacToeButton]
    X_TYPE = -1
    O_TYPE = 1
    TIE = 2

    players: dict
    current: int

    def __init__(self, player1: Member, player2: Member):
        super().__init__()
        self.players = {self.X_TYPE: player1, self.O_TYPE: player2}
        self.current = self.X_TYPE
        self.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

        for x_coord in range(3):
            for y_coord in range(3):
                self.add_item(TicTacToeButton(x_coord, y_coord))
        logger.info(_('TICTACTOE_LOG_START'), {'p1': self.players[1], 'p2': self.players[-1]})

    def get_winner(self) -> int:
        for player_type in [self.O_TYPE, self.X_TYPE]:
            # check horizontal
            for across in self.board:
                value = sum(across)
                if value == player_type * 3:
                    return player_type

            # check vertical
            for line in range(3):
                value = self.board[0][line] + self.board[1][line] + self.board[2][line]
                if value == player_type * 3:
                    return player_type

            # check diagonals
            diag1 = self.board[0][2] + self.board[1][1] + self.board[2][0]
            diag2 = self.board[0][0] + self.board[1][1] + self.board[2][2]
            if diag1 == player_type * 3 or diag2 == player_type * 3:
                return player_type

        # check if board is full
        if all(i != 0 for row in self.board for i in row):
            return self.TIE
        return None

    async def update(self, inter: Interaction, button: TicTacToeButton) -> None:
        if inter.user == self.players[self.current]:
            if self.current == self.X_TYPE:
                button.label = 'X'
                button.style = ButtonStyle.danger
            else:
                button.label = 'O'
                button.style = ButtonStyle.success
            button.disabled = True
            self.board[button.y_coord][button.x_coord] = self.current
            self.current = self.current * -1
        current_player = self.players[self.current]
        content = _('TICTACTOE_ROUND_PLAYER') % {'player': current_player.mention}

        winner = self.get_winner()
        if winner is not None:
            if winner in self.players:
                content = _('TICTACTOE_WINNER') % {'player': self.players[winner].mention}
                logger.info(_('TICTACTOE_LOG_WINNER'), {
                    'winner': self.players[winner],
                    'loser': self.players[winner * -1]
                })
            else:
                content = _('TICTACTOE_TIE')
                logger.info(_('TICTACTOE_LOG_TIE'), {
                    'p1': self.players[1],
                    'p2': self.players[-1]
                })

            for child in self.children:
                child.disabled = True
            self.stop()

        await inter.response.edit_message(content=content, view=self)


class TicTacToeCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @app_commands.command()
    async def tictactoe(self, interaction: Interaction, player2: Member) -> None:
        if player2.bot:
            logger.info(_('TICTACTOE_LOG_PLAYER2_BOT'), {'user': interaction.user})
            await interaction.response.send_message(
                content=_('TICTACTOE_PLAYER2_BOT'),
                ephemeral=True
            )
        else:
            await interaction.response.send_message(
                content= _('TICTACTOE_ROUND_PLAYER') % {'player': interaction.user.mention},
                view=TicTacToe(player1=interaction.user, player2=player2)
            )
