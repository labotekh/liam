#!/bin/sh

printf "%s" "waiting for database ..."
while ! nc -z db 5432
do
	sleep 1
    printf "%c" "."
done
printf "\n%s\n"  "Database is online"

mkdir -p /code/files/logs

chown -R 8135:8135 /code
# run CMD
exec runuser -u runner -g runner -- $@
