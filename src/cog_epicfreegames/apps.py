"""Configuration for cog_epicfreegames app. """

from django.apps import AppConfig


class CogEpicFreeGamesConfig(AppConfig):
    """Configuration object for cog_epicfreegames app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_epicfreegames'
