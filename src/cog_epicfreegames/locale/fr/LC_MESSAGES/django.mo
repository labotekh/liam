��          �      �       H     I     ]     |     �  #   �  $   �      �          3     O     g     �     �  ;  �  3   �          %  &   6  J   ]  G   �  U   �  k   F     �     �     �     �     �                                   
                   	           EFG_CHANNEL_MISSING EFG_CONFIGURATION_ACTIVE_FIELD EFG_CONFIGURATION_CHANNEL_FIELD EFG_CONFIGURATION_TITLE EFG_CREATE_THREAD_NEW_CONFIGURATION EFG_CREATE_THREAD_ON_CURRENT_CHANNEL EFG_CREATE_THREAD_ON_NEW_CHANNEL EFG_NO_CONFIGURATION EFG_REMINDER_END_DATE_FIELD EFG_REMINDER_LINK_FIELD EFG_REMINDER_START_DATE_FIELD EFG_REMINDER_TITLE_PREFIX EFG_THREAD_NAME Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-03 15:48+2
Last-Translator: Imotekh <imotekh@protonmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Le channel configuré n'a pas pu être récupéré. Actif Channel / Thread Configuration de rappel Epic Free Game La configuration est sauvegardée et un fil a été créer sur le channel. Un fil a été créer, le précédent n'ayant pas pu être récupéré. Un fil a été créer sur le nouveau channel, considérez la suppression de l'ancien. Il n'y a pas de configuration exitante, considérez d'utiliser l'option "config_channel"pour en créer une. Date de fin Lien Date de début Epic Free Game Epic Free Games 