from datetime import datetime, timedelta
import logging
from typing import Optional

from asgiref.sync import sync_to_async

from discord import app_commands, Interaction, TextChannel, Embed
from discord.ext import commands, tasks

from django.utils.translation import gettext as _

import requests

from cog_epicfreegames import EPIC_FREE_GAMES_API, EPIC_GAME_WEB
from cog_epicfreegames.models import EpicFreeGamesThread

logger = logging.getLogger('default')


class EpicFreeGamesCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.epic_free_games_reminder.start() # pylint: disable=no-member

    def _create_game_embed(self, game_data: dict) -> Optional[Embed]:
        """ Create the embed for a free game and return it if the start date is within a day. """
        embed = None
        dates = game_data['promotions']['promotionalOffers'][0]['promotionalOffers'][0]
        start_date = datetime.strptime(dates['startDate'], '%Y-%m-%dT%H:%M:%S.%fZ')
        end_date = datetime.strptime(dates['endDate'], '%Y-%m-%dT%H:%M:%S.%fZ')
        # check the start date is within a day
        if -timedelta(days=1) < (start_date - datetime.now()) <= timedelta(days=1):
            game_url = EPIC_GAME_WEB + str(game_data["productSlug"])
            embed = Embed(
                title=_('EFG_REMINDER_TITLE_PREFIX') + f" | {game_data['title']}",
                description=game_data['description'],
                colour=0xe5bf04
            )
            embed.set_image(url=game_data['keyImages'][1]['url'])
            embed.add_field(
                name=_('EFG_REMINDER_START_DATE_FIELD'),
                value=f'<t:{int(start_date.timestamp())}:R>'
            )
            embed.add_field(
                name=_('EFG_REMINDER_END_DATE_FIELD'),
                value=f'<t:{int(end_date.timestamp())}:R>'
            )
            embed.add_field(
                name=_('EFG_REMINDER_LINK_FIELD'),
                value=f'[Web]({game_url})'
            )
        return embed

    @tasks.loop(hours=24)
    async def epic_free_games_reminder(self) -> None:
        """ Send a reminder to all registered threads. """
        # retrieve free games data from API
        free_games = requests.get(EPIC_FREE_GAMES_API, timeout=5).json()
        free_games = free_games['data']['Catalog']['searchStore']['elements']
        free_games = filter(lambda game_data: game_data['title'] != 'Mystery Game', free_games)
        free_games = filter(lambda game_data: game_data['promotions'], free_games)
        free_games = filter(lambda game_data: game_data['promotions']['promotionalOffers'], free_games)

        # format games into embeds list
        embeds = []
        for game_data in free_games:
            embed = self._create_game_embed(game_data)
            if embed:
                embeds.append(embed)

        # retrieve configurations
        threads = await sync_to_async(list)(
            await sync_to_async(EpicFreeGamesThread.objects.filter)(active=True) # pylint: disable=no-member
        )
        # send embeds to configured threads
        for thread in threads:
            guild = await self.bot.fetch_guild(int(thread.guild_id))
            if guild:
                thread = await guild.fetch_channel(int(thread.thread_id))
                if thread:
                    await thread.send(embeds=embeds)

    @app_commands.command()
    @app_commands.checks.has_permissions(administrator=True)
    async def epic_free_games(self, interaction: Interaction,
        config_channel: Optional[TextChannel] = None, active: Optional[bool] = None) -> None:
        """ Configure and display configuration of Epic Free Games reminder. """
        action = ''
        channel = None
        thread = None
        embed = Embed(title=_('EFG_CONFIGURATION_TITLE'), colour=0xe5bf04)
        # retrieve configuration of the guild
        thread_obj = await sync_to_async((
            await sync_to_async(EpicFreeGamesThread.objects.filter)( # pylint: disable=no-member
                guild_id=str(interaction.guild_id)
            )
        ).first)()

        if config_channel:
            if thread_obj:
                # load channel and thread from Discord
                channel = interaction.guild.get_channel_or_thread(int(thread_obj.channel_id))
                thread = interaction.guild.get_channel_or_thread(int(thread_obj.thread_id))
                # check if channel has to be changed
                if config_channel.id != channel.id:
                    thread = await config_channel.create_thread(name=_('EFG_THREAD_NAME'))
                    action = _('EFG_CREATE_THREAD_ON_NEW_CHANNEL')
                    thread_obj.channel_id = str(channel.id)
                    thread_obj.thread_id = str(thread.id)
                elif not thread:
                    thread = await config_channel.create_thread(name=_('EFG_THREAD_NAME'))
                    action = _('EFG_CREATE_THREAD_ON_CURRENT_CHANNEL')
                    thread_obj.thread_id = str(thread.id)
            else:
                thread = await config_channel.create_thread(name=_('EFG_THREAD_NAME'))
                action = _('EFG_CREATE_THREAD_NEW_CONFIGURATION')
                thread_obj = EpicFreeGamesThread(
                    guild_id=str(interaction.guild_id),
                    channel_id=str(config_channel.id),
                    thread_id=str(thread.id)
                )
            channel = config_channel
        else:
            if not thread_obj:
                action = _('EFG_NO_CONFIGURATION')
            else:
                # load channel and thread from Discord
                channel = interaction.guild.get_channel_or_thread(int(thread_obj.channel_id))
                thread = interaction.guild.get_channel_or_thread(int(thread_obj.thread_id))

                if not channel:
                    action = _('EFG_CHANNEL_MISSING')
                else:
                    if not thread:
                        thread = await channel.create_thread(name='Epic Free Games')
                        thread_obj.thread_id = str(thread.id)
                        action = _('EFG_CREATE_THREAD_ON_CURRENT_CHANNEL')

        if thread_obj and active is not None:
            thread_obj.active = active
        await sync_to_async(thread_obj.save)()

        if action:
            embed.description = action
        if thread and channel:
            embed.add_field(
                name=_('EFG_CONFIGURATION_CHANNEL_FIELD'),
                value=f'{channel.mention} / {thread.mention}'
            )
        if thread_obj:
            embed.add_field(
                name=_('EFG_CONFIGURATION_ACTIVE_FIELD'),
                value='✅' if thread_obj.active else '❌'
            )
        await interaction.response.send_message(embed=embed, ephemeral=True)

    @epic_free_games.error
    async def on_epic_free_games_error(self, interaction: Interaction,
        error: app_commands.AppCommandError) -> None:
        """ "Display missing permission error to user. """
        if isinstance(error, app_commands.MissingPermissions):
            await interaction.response.send_message(str(error), ephemeral=True)
        else:
            raise error
