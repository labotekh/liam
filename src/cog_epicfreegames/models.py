from django.db import models


class EpicFreeGamesThread(models.Model):
    """The configuration for Epic Free Games reminder on a server. """

    guild_id = models.CharField(max_length=20, unique=True)
    channel_id = models.CharField(max_length=20, unique=True)
    thread_id = models.CharField(max_length=20, unique=True)
    active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return str(self.guild_id)
