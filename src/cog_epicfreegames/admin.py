"""
Administration panel configuration for cog_epicfreegames
"""

from django.contrib import admin

from cog_epicfreegames.models import EpicFreeGamesThread


@admin.register(EpicFreeGamesThread)
class AdminEpicFreeGamesThread(admin.ModelAdmin):
    """Settings to display `EpicFreeGamesThread` in administration panel. """

    list_display = ('guild_id', 'channel_id', 'thread_id', 'active')
    list_filter = ('active',)
    ordering = ('guild_id',)
    search_fields = ('guild_id', 'channel_id', 'thread_id')
    fields = ('guild_id', 'channel_id', 'thread_id', 'active')
