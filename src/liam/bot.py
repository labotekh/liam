import logging
import os
import pydoc

from discord import Intents, Message, Interaction, Object
from discord.ext import commands

from django.conf import settings
from django.utils.translation import gettext as _

logger = logging.getLogger('default')


class LiamBot(commands.Bot):
    """ Client for LIAM Discord Bot. """

    def __init__(self, *, intents: Intents, application_id: int):
        super().__init__('>', intents=intents, application_id=application_id)

    async def setup_hook(self) -> None:
        # register cogs
        for cog in settings.COGS:
            await bot.add_cog(pydoc.locate(cog)(bot))

        if settings.DEBUG:
            # synchronise app commands to Discord
            self.tree.copy_global_to(guild=Object(id=os.environ.get('TEST_GUILD_ID')))
            await self.tree.sync(guild=Object(id=os.environ.get('TEST_GUILD_ID')))
        else:
            await self.tree.sync()


default_intents = Intents.all()
bot = LiamBot(intents=default_intents, application_id=os.environ.get('APPLICATION_ID'))


@bot.event
async def on_ready() -> None:
    logger.info(_('LIAM_LOGGED_IN_LOG'), {'user': bot.user, 'id': bot.user.id})


@bot.tree.context_menu()
async def ratio(interaction: Interaction, message: Message) -> None:
    await interaction.response.send_message(_('LIAM_RATIO_MESSAGE'), ephemeral=True)
    await message.add_reaction('🇷')
    await message.add_reaction('🇦')
    await message.add_reaction('🇹')
    await message.add_reaction('🇮')
    await message.add_reaction('🇴')
    logger.info(_('LIAM_RATIO_LOG'), {'user': interaction.user, 'target': message.author})
