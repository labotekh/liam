from os import listdir
from os.path import isdir, join

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = 'Merge all translation files for all local apps'

    FINAL_FILE = 'django.po'
    TRADFOLDER = 'locale'
    SUBFOLDER = 'LC_MESSAGES'

    def _merge(self, lang_folder):
        """ Merge all .po in django.po. """
        destination = join(lang_folder, self.SUBFOLDER, self.FINAL_FILE)
        with open(destination, 'w', encoding="utf-8") as to_write:
            # iterate over all to traduction file to merge them
            for trad_file in listdir(join(lang_folder, self.SUBFOLDER)):
                if trad_file.endswith('.po') and trad_file != self.FINAL_FILE:
                    # copy content of .po in FINAL_FILE
                    source = join(lang_folder, self.SUBFOLDER, trad_file)
                    with open(source, 'r', encoding="utf-8") as to_read:
                        to_write.write(to_read.read())

    def handle(self, *args, **options):
        """ Command entrypoint. """
        for app in settings.INSTALLED_APPS: # pylint: disable=duplicate-code
            locale_folder = join(app, self.TRADFOLDER)
            # check app is locally installed
            if isdir(app) and isdir(locale_folder):
                for language in settings.LANGUAGES: # pylint: disable=duplicate-code
                    lang_folder = join(locale_folder, language[0])
                    # check if language has a traduction folder
                    if isdir(lang_folder) and isdir(join(lang_folder, self.SUBFOLDER)):
                        self._merge(lang_folder)
