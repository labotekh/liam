import os

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = 'Create a default admin'

    def handle(self, *args, **options):
        """ Command entrypoint. """
        username = os.environ.get('DEFAULT_ADMIN_USERNAME')
        password = os.environ.get('DEFAULT_ADMIN_PASSWORD')
        already_exists = User.objects.filter(username=username, is_superuser=True).first()

        if not already_exists:
            User.objects.create_superuser(username, password=password)
