from django.conf import settings
from django.core.management.base import BaseCommand

from liam.bot import bot

class Command(BaseCommand):

    help = 'Start Discord bot'

    def handle(self, *args, **kwargs): # pylint: disable=unused-argument
        """ Command entrypoint. """
        bot.run(settings.TOKEN_DISCORD)
