from os import listdir, remove
from os.path import isdir, join, isfile

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = 'Split back translations and clean translation folders'

    TRADFOLDER = 'locale'
    SUBFOLDER = 'LC_MESSAGES'

    FINAL_FILE = 'django.mo'
    TO_DELETE = 'django.po'
    PATTERN = '# file : '

    def _clean_up(self, lang_folder):
        """ Delete trailing .mo files except django.mo. """
        for trad_file in listdir(join(lang_folder, self.SUBFOLDER)):
            if trad_file.endswith('.mo') and trad_file != self.FINAL_FILE:
                remove(join(lang_folder, self.SUBFOLDER, trad_file))
        if isfile(join(lang_folder, self.SUBFOLDER, self.TO_DELETE)):
            remove(join(lang_folder, self.SUBFOLDER, self.TO_DELETE))

    def _split(self, lang_folder):
        """ Split django.po in multiple .po files. """
        source = join(lang_folder, self.SUBFOLDER, self.TO_DELETE)
        with open(source, 'r', encoding="utf-8") as to_read:
            dest_file = None
            # iterate over all lines
            for line in to_read.readlines():
                if line.startswith(self.PATTERN):
                    filename = line.replace(self.PATTERN, '').replace('\n', '').replace('\r', '')
                    destination = join(lang_folder, self.SUBFOLDER, filename)
                    dest_file = open(destination, 'w', encoding="utf-8") # pylint: disable=consider-using-with
                    # copy content of TO_DELETE to other .po
                if dest_file:
                    dest_file.write(line)

    def handle(self, *args, **options):
        """ Command entrypoint. """
        for app in settings.INSTALLED_APPS: # pylint: disable=duplicate-code
            locale_folder = join(app, self.TRADFOLDER)
            # check app is locally installed
            if isdir(app) and isdir(locale_folder):
                for language in settings.LANGUAGES: # pylint: disable=duplicate-code
                    lang_folder = join(locale_folder, language[0])
                    # check if language has a traduction folder
                    if isdir(lang_folder) and isdir(join(lang_folder, self.SUBFOLDER)):
                        self._split(lang_folder)
                        self._clean_up(lang_folder)
