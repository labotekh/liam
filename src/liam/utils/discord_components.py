import logging

from discord import Interaction, ButtonStyle
from discord.ui import Button

from django.utils.translation import gettext as _

logger = logging.getLogger('default')


class ActionButton(Button):

    action: callable

    def __init__(self, emoji: str, action: callable, label: str = None, row: int = 4):
        self.action = action
        super().__init__(emoji=emoji, label=label, style=ButtonStyle.secondary, row=row)

    async def callback(self, interaction: Interaction) -> None:
        await self.view.call_action(interaction, self.action)


class SwitchButton(Button):

    attribute: str
    label_id: str

    def __init__(self, emoji: str, attribute: str, label: str = None, row: int = 4):
        self.attribute = attribute
        self.label_id = label
        super().__init__(emoji=emoji, style=ButtonStyle.secondary, row=row)

    def update(self) -> None:
        setattr(self.view.data, self.attribute, not getattr(self.view.data, self.attribute))
        emoji = '✅' if getattr(self.view.data, self.attribute) else '❌'
        self.label = _(self.label_id) % {'show': emoji}

    async def callback(self, interaction: Interaction) -> None:
        self.update()
        await self.view.update(interaction)
