"""Configuration for liam app. """

from django.apps import AppConfig

class LiamConfig(AppConfig):
    """Configuration object for liam app. """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'liam'
