from unidecode import unidecode

from discord import Interaction, TextStyle
from discord.ui import TextInput, Modal

from django.utils.translation import gettext as _


class LetterModal(Modal):

    title = _('HANGMAN_LETTER_MODAL_TITLE')
    letter = TextInput(
        label=_('HANGMAN_LETTER_MODAL_LABEL'),
        style=TextStyle.short, min_length=1, max_length=1
    )

    def __init__(self, view):
        super().__init__()
        self.view = view

    async def on_submit(self, inter: Interaction) -> None: # pylint: disable=arguments-differ
        message = self.view.data.check_letter(inter.user, unidecode(self.letter.value.lower()))
        await self.view.update(inter, message)


class WordModal(Modal):

    title = _('HANGMAN_WORD_MODAL_TITLE')
    word = TextInput(
        label=_('HANGMAN_WORD_MODAL_LABEL'),
        style=TextStyle.short, min_length=1, max_length=100
    )

    def __init__(self, view):
        super().__init__()
        self.view = view

    async def on_submit(self, inter: Interaction) -> None: # pylint: disable=arguments-differ
        message = self.view.data.check_word(inter.user, unidecode(self.word.value.lower()))
        await self.view.update(inter, message)
