import logging
from typing import List

from discord import Embed, Member

from django.utils.translation import gettext as _
from cog_hangman import HANGMAN_STATES

logger = logging.getLogger('default')


def pre_check(func: callable) -> callable:
    def check_wrapper(*args):
        game, user = args[0], args[1]
        message = ''
        if not game.ongoing:
            logger.info(_('HANGMAN_LOG_STATUS_FINISH'), locals())
            message = _('HANGMAN_STATUS_FINISH')
        elif game.author == user:
            logger.info(_('HANGMAN_LOG_OWNER_CANT_PLAY'), locals())
            message = _('HANGMAN_OWNER_CANT_PLAY')
        else:
            message = func(*args)
        return message
    return check_wrapper


class HangmanGame:

    author: Member
    to_guess: str
    letters: List[str]
    words: List[str]
    lifepoints: int
    ongoing: bool

    def __init__(self, to_guess: str, author: Member):
        self.author = author
        self.to_guess = to_guess
        self.letters = []
        self.words = []
        self.lifepoints = len(HANGMAN_STATES) -1
        self.ongoing = True
        self.description = ''

    def _lose_lifepoint(self) -> None:
        self.lifepoints -= 1
        if self.lifepoints == 0:
            self.description = _('HANGMAN_STATUS_LOSE')
            self.ongoing = False

    @property
    def _guessing_string(self) -> str:
        guess_string = ''
        for letter in self.to_guess:
            if not letter.isalpha() or letter in self.letters or not self.ongoing:
                guess_string += letter + ' '
            else:
                guess_string += '_ '
        return f'```{guess_string}```'

    @property
    def _sketch(self) -> str:
        return f'```{HANGMAN_STATES[len(HANGMAN_STATES) - self.lifepoints -1]}```'

    @property
    def embed(self) -> Embed:
        embed = Embed(description=self.description, color=0xdbb706)
        if self.author.nick:
            embed.set_author(name=self.author.nick, icon_url=self.author.display_avatar.url)
        else:
            embed.set_author(name=self.author.name, icon_url=self.author.display_avatar.url)
        embed.add_field(name=self._guessing_string, value=self._sketch)
        if self.letters:
            embed.add_field(
                name=_('HANGMAN_FIELD_LETTERS'),
                value=', '.join(self.letters), inline=True
            )
        if self.words:
            embed.add_field(
                name=_('HANGMAN_FIELD_WORDS'),
                value=', '.join(self.words), inline=True
            )
        return embed

    @pre_check
    def check_letter(self, user: Member, letter: str) -> str:
        message = None
        word = self.to_guess
        if len(letter) != 1 or letter in self.letters or not letter.isalpha():
            message = _('HANGMAN_INVALID_LETTER') % locals()
            logger.info(_('HANGMAN_LOG_INVALID_LETTER'), locals())
        else:
            self.letters.append(letter)
            if letter not in word:
                self._lose_lifepoint()
                message = _('HANGMAN_LETTER_IS_NOT_PRESENT') % locals()
                logger.info(_('HANGMAN_LOG_LETTER_IS_NOT_PRESENT'), locals())
            else:
                message = _('HANGMAN_LETTER_IS_PRESENT') % locals()
                logger.info(_('HANGMAN_LOG_LETTER_IS_PRESENT'), locals())
                if all(character in self.letters for character in word):
                    message = _('HANGMAN_LETTER_ALL_FOUND') % locals()
                    logger.info(_('HANGMAN_LOG_LETTER_ALL_FOUND'), locals())
                    self.description = _('HANGMAN_STATUS_WINNER') % {'winner': user.mention}
                    self.ongoing = False
        return message

    @pre_check
    def check_word(self, user: Member, word: str) -> str:
        message = ''
        if word in self.words:
            message = _('HANGMAN_INVALID_WORD') % locals()
            logger.info(_('HANGMAN_LOG_INVALID_WORD'), locals())
        else:
            self.words.append(word)
            if word == self.to_guess:
                logger.info(_('HANGMAN_LOG_STATUS_WINNER'), locals())
                self.description = _('HANGMAN_STATUS_WINNER') % {'winner': user.mention}
                self.ongoing = False
                message = _('HANGMAN_CORRECT_WORD')
            else:
                self._lose_lifepoint()
                message = _('HANGMAN_WRONG_WORD') % locals()
                logger.info(_('HANGMAN_LOG_WRONG_WORD'), locals())
        return message

    def cancel(self) -> None:
        user = self.author # pylint: disable=possibly-unused-variable
        self.ongoing = False
        self.description = _('HANGMAN_STATUS_STOPPED')
        logger.info(_('HANGMAN_LOG_STATUS_STOPPED'), locals())
