"""Configuration for cog_hangman app. """

from django.apps import AppConfig


class CogHangmanConfig(AppConfig):
    """Configuration object for cog_hangman app. """

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_hangman'
