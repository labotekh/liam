import logging
from unidecode import unidecode

from discord import app_commands, Interaction
from discord.ext import commands

from django.utils.translation import gettext as _

from cog_hangman.view import HangmanView

logger = logging.getLogger('default')


class HangmanCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @app_commands.command()
    async def hangman(self, inter: Interaction, to_guess: str) -> None:

        to_guess = unidecode(to_guess.lower())
        logger.info(_('HANGMAN_LOG_GAME_CREATED'), {'user': inter.user, 'word': to_guess})
        view = HangmanView(to_guess, inter.user)
        await inter.response.send_message(embed=view.data.embed, view=view)
