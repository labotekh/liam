import logging
from typing import Optional

from discord import Interaction, Member
from discord.ui import View

from django.utils.translation import gettext as _

from liam.utils.discord_components import ActionButton

from cog_hangman import components
from cog_hangman.game import HangmanGame

logger = logging.getLogger('default')


class HangmanView(View):

    data        : HangmanGame

    b_cancel    : ActionButton
    b_letter    : ActionButton
    b_word      : ActionButton

    def __init__(self, to_guess: str, author: Member):
        super().__init__(timeout=None)
        self.data = HangmanGame(to_guess, author)
        self.init_components()
        self.add_components()

    # ======================================================================================= #
    #                                     COMPONENT METHODS                                   #
    # ======================================================================================= #

    def init_components(self):

        self.b_cancel = ActionButton('❌', self.cancel,       _('HANGMAN_CANCEL_LABEL'), row=1)
        self.b_letter = ActionButton('🔍', self.check_letter, _('HANGMAN_LETTER_LABEL'), row=1)
        self.b_word   = ActionButton('🕵️', self.check_word,   _('HANGMAN_WORD_LABEL'),   row=1)

    def add_components(self):
        for component in [self.b_cancel, self.b_letter, self.b_word]:
            self.add_item(component)

    # ======================================================================================= #
    #                                     WRAPPING METHODS                                    #
    # ======================================================================================= #

    async def call_action(self, inter: Interaction, action: str) -> None:
        await action(inter)

    async def update(self, inter: Interaction, message: Optional[str] = None) -> None:
        embed = self.data.embed
        if message and embed:
            embed.set_footer(text=f'{inter.user.display_name} : {message}')
        if not self.data.ongoing:
            self.timeout = 0
            for component in self.children:
                component.disabled = True
        await inter.response.edit_message(embed=embed, view=self)

    # ======================================================================================= #
    #                                     BUTTON ACTIONS                                      #
    # ======================================================================================= #

    async def cancel(self, inter: Interaction) -> None:
        user = inter.user
        if user == self.data.author:
            self.data.cancel()
            self.timeout = 0
            for component in self.children:
                component.disabled = True
            await self.update(inter)
        else:
            logger.info(_('HANGMAN_LOG_CANT_STOP_GAME_NOT_OWNED'), locals())
            await self.update(inter, _('HANGMAN_CANT_STOP_GAME_NOT_OWNED'))

    async def check_letter(self, inter: Interaction) -> None:
        await inter.response.send_modal(components.LetterModal(view=self))

    async def check_word(self, inter: Interaction) -> None:
        await inter.response.send_modal(components.WordModal(view=self))
