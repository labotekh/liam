"""
Generate a changelog entry file in the correct location.

Automatically stages the file and amends the previous commit if the `--amend`
argument is used.

Also generate CHANGELOG.md if option --generate is used.

-----------------------------------------------------------------------------------
usage: changelog.py [-h] [--title TITLE] [-m MERGE_REQUEST]
                    [-t {new,fix,edit,depr,styl,del,sec,perf,meta,arch,upda,doc,dbg,test,oth}]
                    [--tag TAG] [-a] [-c] [-n] [-u] [-g] [-f] [-v]

Generate a changelog entry file in the correct location.
Automatically stages the file and amends the previous commit if the `--amend`argument is used.

optional arguments:
  -h, --help            show this help message and exit
  --title TITLE         Description of the entry
  -m MERGE_REQUEST, --merge-request MERGE_REQUEST
                        Merge Request ID
  -t {new,fix,edit,depr,styl,del,sec,perf,meta,arch,upda,doc,dbg,test,oth},
  --type {new,fix,edit,depr,styl,del,sec,perf,meta,arch,upda,doc,dbg,test,oth}
                        The category of the change
  --tag TAG             The tag to link to (look at "git tag -l" to have the list)
  -a, --amend           Amend the previous commit
  -c, --commit          Create a commit with the new entry
  -n, --dry-run         Don't actually write anything, just print
  -u, --git-username    Use Git user.name configuration as the author
  -g, --generate        Generate CHANGELOG.md file and stage on latest tag unstaged changes
  -f, --force           Force to delete old version of file
  -v, --verbose         Display steps of execution
"""

from argparse import ArgumentParser
from subprocess import Popen, PIPE
from os import listdir, walk, system, makedirs, replace
from os.path import isfile, join, exists, dirname, basename
from yaml import dump, load, FullLoader

TYPES = {
    'new': 'New feature',
    'fix': 'Bug fix',
    'edit': 'Feature change',
    'depr': 'Deprecation',
    'styl': 'Refactorisation',
    'del': 'Feature removal',
    'sec': 'Security fix',
    'perf': 'Performance improvement',
    'meta': 'Meta',
    'arch': 'Architecture',
    'upda': 'Version or library update',
    'doc': 'Documentation',
    'dbg': 'Debug',
    'test': 'Test',
    'oth': 'Other',
}

CHANGELOG_DIR = 'changelog'
CHANGELOG_FILE = 'CHANGELOG.md'

class SimpleLogger:
    """
    A short custom logger to print info in more fancy way and use --verbose option
    """

    def __init__(self, verbose):
        self.verbose = verbose

    def debug(self, message):
        """
        Print DEBUG message in gray
        """

        if self.verbose:
            print("\033[90m[DEBUG] {}\033[0m".format(message))

    def info(self, message, color=None):
        """
        Print informative message with desired color
        """

        if not color:
            print(message)
        else:
            print("\033[{}m{}\033[0m".format(color, message))

    def two_info(self, message_1, color_1, message_2, color_2):
        """
        Print a mesage with two different colors
        """

        print("\033[{}m{}\033[0m\033[{}m{}\033[0m".format(color_1, message_1, color_2, message_2))

    def error(self, message):
        """
        Print ERROR message
        """

        print("\033[91m[ERROR] {}\033[0m".format(message))

    def success(self, message):
        """
        Print SUCCESS message
        """

        print("\033[92m[SUCCESS] {}\033[0m".format(message))

parser = ArgumentParser(description=
    "Generate a changelog entry file in the correct location."
    "Automatically stages the file and amends the previous commit if the `--amend`"
    "argument is used."
)
parser.add_argument('--title', help="Description of the entry")
parser.add_argument('-m', '--merge-request', type=int, help="Merge Request ID")
parser.add_argument('-t', '--type', choices=TYPES.keys(), help="The category of the change")
parser.add_argument('--tag', help="The tag to link to (look at \"git tag -l\" to have the list)")
parser.add_argument('-n', '--dry-run', action="store_true", help="Don't actually write anything, just print")
parser.add_argument('-u', '--git-username', action="store_true", help='Use Git user.name configuration as the author')
parser.add_argument('-g', '--generate', action="store_true", help="Generate CHANGELOG.md file and stage on latest tag unstaged changes")
parser.add_argument('-f', '--force', action="store_true", help="Force to delete old version of file")
parser.add_argument('-v', '--verbose', action="store_true", help="Display steps of execution")
args = parser.parse_args()

logger = SimpleLogger(args.verbose)

def shell(command):
    """
    Execute a command in host shell
    """

    logger.debug("Executing command : {}".format(command))
    cmd = Popen(command, stdout=PIPE, stderr=PIPE)
    stdout, err = cmd.communicate()
    cmd.wait()
    result = stdout.decode('UTF-8')
    error = err.decode('UTF-8')
    if error:
        raise Exception(error)
    return result

def get_changelog_subdirs():
    """
    Get subdirectories of changelog dir.
    """

    directory_list = None
    for (dirpath, dirnames, filenames) in walk(CHANGELOG_DIR):
        directory_list = dirnames
        break
    return directory_list

def print_types():
    """
    Print TYPES dict to terminal
    """

    logger.info("\nPlease choose one of the following type by providing matching index.", color=90)
    i = 1
    for type_key in TYPES:
        logger.two_info(i, 33, ". " + TYPES[type_key], 0)
        i += 1

class ChangeLogPart:
    """
    A utility class to generate a changelog entry file
    """

    def __init__(self, args):
        """
        Use CLI arguments to generate the entry file
        """

        self._set_title(args)
        self._set_author(args)
        self._set_type(args)
        self._set_merge_request(args)
        file_path = self._gen_file(args.dry_run, args.tag, args.force)
        logger.success("Entry created")

    def _get_commit_msg(self):
        """
        Compute proposed commit message
        """

        return "[{}] {}".format(self.type, self.title.lower())

    def _set_merge_request(self, args):
        """
        Set merge request number for the entry.
        """

        if args.merge_request:
            self.merge_request = args.merge_request
        else:
            self.merge_request = ''

    def _set_author(self, args):
        """
        Set author for the entry.
        """

        if args.git_username:
            username = shell("git config user.name").replace("\n", '')
            email = shell("git config user.email").replace("\n", '')
            self.author = "{} <{}>".format(username, email)
            logger.debug("Retrieving user from gitlab configuration : {}".format(self.author))
        else:
            self.author = ''

    def _set_title(self, args):
        """
        Set title from CLI argument or take last commit message if not provided
        """

        tmp_title = args.title
        if not tmp_title:
            tmp_title = shell("git log --format=%s -1")
        self.title = tmp_title.split('\n')[0]

    def _set_type(self, args):
        """
        Set type according to CLI argument or ask it in terminal
        """

        if args.type:
            self.type = args.type
        else:
            print_types()
            index = int(input('> '))-1
            if 0 <= index < len(TYPES):
                self.type = list(TYPES.keys())[index]
            else:
                raise Exception("Invalid index provided")
        logger.debug("Setting type to : {} ({})".format(TYPES[self.type], self.type))

    def _get_file_path(self, tag):
        """
        Compute file path from tag provided (if present) and title of the entry.
        """

        dir_path = join(CHANGELOG_DIR, 'unstaged')
        if tag:
            if tag not in shell("git tag -l").split('\n'):
                raise Exception("Provided tag ({}) does not exist in this repository".format(tag))
            dir_path = join(CHANGELOG_DIR, tag)
        sanitized_title = self.title.replace(' ', '_').replace('\'', '_').replace('/', '_').lower()
        filename = "{}.yml".format(sanitized_title)
        file_path = join(dir_path, filename)
        logger.debug("Computed file_path to : {}".format(file_path))
        return file_path

    def _gen_file(self, dry_run, tag, force):
        """
        Generate entry file in YAML and write it to FS.
        """

        data = {
            "title": self.title,
            "author": self.author,
            "type": self.type,
            "merge-request": self.merge_request,
        }
        yaml_data = dump(data)
        file_path = self._get_file_path(tag)

        logger.info("\n------------------------------------------------------------", color=90)
        logger.info(file_path, color=34)
        logger.info("------------------------------------------------------------", color=90)
        logger.two_info("title", 33, ": " + self.title, 0)
        logger.two_info("author", 33, ": " + self.author, 0)
        logger.two_info("merge-request", 33, ": " + self.merge_request, 0)
        logger.two_info("type", 33, ": {}\n".format(self.type), 0)

        if not dry_run:
            if exists(file_path) and not force:
                raise Exception("File already exist use -f to erase")
            elif exists(file_path) and force:
                logger.debug("Erasing older file with new version")
            if not exists(dirname(file_path)):
                makedirs(dirname(file_path))

            logger.debug("Writing YAML data to computed file")
            file = open(file_path, "w")
            file.write(yaml_data)
            file.close()
        return file_path

class ChangeLog:
    """
    A utility class to generate main changelog file from entries uner changelog/.
    """

    def __init__(self):
        """
        Start generation
        """

        tag_list = shell("git tag -l").split('\n')[:-1]

        if not tag_list:
            logger.error("There is no tag to use, please add a tag before generating changelog.")
        else:
            tag_list.append('unstaged')
            dir_list = get_changelog_subdirs()
            
            with open(CHANGELOG_FILE, "w") as file:
                self._write_header(file)

                for tag in tag_list:
                    if tag in dir_list:
                        self._write_tag_header(tag_list, tag, dir_list, file)

                        path = join(CHANGELOG_DIR, tag)
                        onlyfiles = [join(path, f) for f in listdir(path) if isfile(join(path, f))]

                        entry_list = self._build_entry_list(onlyfiles, tag, tag_list)
                        self._write_entries(file, entry_list)
            logger.success("CHANGELOG generated")

    def _write_header(self, file):
        """
        Write header of the file
        """

        logger.debug("Writing CHANGELOG header")
        file.write("# Changelog\n\n")
        file.write("This file is automatically generated please check documentation to add an entry.\n\n")

    def _write_tag_header(self, tag_list, tag, dir_list, file):
        """
        Write each tag header with it's matching date
        """

        logger.debug("Writing tag header")
        if tag == 'unstaged' and tag_list[-2] in dir_list:
            raise Exception('There is no new tag to save unstaged changes')
        elif tag == 'unstaged':
            tag_date = shell("git log -1 --format=%ai {}".format(tag_list[-2]))[0:10]
            file.write("## {} ({})\n\n".format(tag_list[-2], tag_date))
        else:
            tag_date = shell("git log -1 --format=%ai {}".format(tag))[0:10]
            file.write("## {} ({})\n\n".format(tag, tag_date))

    def _build_entry_list(self, onlyfiles, tag, tag_list):
        """
        Compute all entry files to create an entry dict sorted by type.
        """

        logger.info("Building entry list")
        entry_list = {}
        for type_str in list(TYPES.keys()):
            entry_list[type_str] = []
        
        for yfile in onlyfiles:
            yaml_file = open(yfile, 'r')
            data = load(yaml_file, Loader=FullLoader)
            entry_list[data["type"]].append(data)
            yaml_file.close()
            
            if tag == 'unstaged':
                filename = join(CHANGELOG_DIR, tag_list[-2], basename(yfile))
                if not exists(dirname(filename)):
                    makedirs(dirname(filename))
                replace(yfile, filename)
        return entry_list

    def _write_entries(self, file, entry_list):
        """
        Write entry list to changelog by type in lists.
        """

        logger.debug("Writing entries to CHANGELOG")
        for entry_type in entry_list:
            if entry_list[entry_type]:

                file.write("### {} ({} change{})\n\n".format(
                    TYPES[entry_type],
                    len(entry_list[entry_type]),
                    's' if len(entry_list[entry_type]) > 1 else ''
                ))

                for entry in entry_list[entry_type]:
                    file.write('- {} {}{}\n'.format(
                        entry['title'],
                        '!' if entry['merge-request'] else '',
                        entry['merge-request'],
                    ))
                file.write('\n')

if __name__ == '__main__':
    
    try:
        if not exists(CHANGELOG_DIR): # Check if changelog dir exists
            logger.info("There is no changelog folder... creating one")
            makedirs(CHANGELOG_DIR)
        if args.generate: # Start CHANGELOG.md generation
            ChangeLog()
        else: # Create a new changelog entry
            ChangeLogPart(args)
    except Exception as e:
        logger.error(e)
