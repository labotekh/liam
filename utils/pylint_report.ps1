if (-Not(Test-Path -Path source\meta -PathType Container)) {
    mkdir source\meta
}
pylint ..\src\ | pylint-json2html -f jsonextended -t report.j2 -o source\meta\pylint.rst
$score = Get-Content source\meta\pylint.rst | Select-String -Pattern "([0-9]+\.[0-9]+) / 10" -AllMatches | ForEach-Object {$_.matches.groups[1].value}
if (Test-Path -Path pylint.svg -PathType Leaf) {
    rm pylint.svg
}
anybadge -l pylint -v $score -f pylint.svg 2=red 4=orange 8=yellow 10=green